#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>



//FUNCTION PROTOTYPES
void mountain(float x0, float y0, float z0, float x1, float y1, float z1, float x2, float y2, float z2, float s);
void init(int argc, char** argv);
int  seed(float x, float y);
void display(void);
void reshape (int w, int h);
void keyboard(unsigned char key, int x, int y);
void animation(int dummy);
void specialKey(int key, int x, int y);
void drawMatrix();



//GLOBALS
float sealevel;
float polysize;
const float mtTileSize=1.0;	//as far as I can tell, each mountain segment is 1fx1f

int FPS = 30;
const int axisSize = 1;	//pertains to desired max draw distance (+-) but limited by
int colorFilled=0;	//0==yes; else=>no


//lookat eye (will not change)
const GLfloat ex	= 0.5;
const GLfloat ey	= 0.0;
const GLfloat ez	= 0.25;
//X'
const GLfloat evx	= -0.5;
const GLfloat evy	= 0.0;
const GLfloat evz	= -0.25;
//Z'
const GLfloat fx	= -0.25;
const GLfloat fy	= 0.0;
const GLfloat fz	= 0.5;
//Y'
const GLfloat gx	= 0.0;
const GLfloat gy	= 1.0;
const GLfloat gz	= 0.0;



//plane's variables (plane is eye)
GLfloat px	= 0.0;	//plane position vector, nose points down -X-Axis
					//because mountains willed it so. Lazy but simple,
					//instead of doing extra rotations just for intuitive
GLfloat yzt	= 0.0;	//purposes. note speeds will be positive because we
GLfloat xzt	= 0.0;		//are moving world
GLfloat xyt	= 0.0;	//repurposed, see below

GLfloat dyzt		= 0.000;	//rotation around x (roll)
const GLfloat dpx	= 0.01;		//plane forward speed does not change
GLfloat dxyt		= 0.000;	//repurposed, see below
GLfloat dxzt		= 0.000;	//rotation (pitch

//acceleration stats
const GLfloat ddyzt		= .7;
const GLfloat ddxzt		= 1;
const GLfloat ddxyt		= 0.005;	//repurposed because original was disorienting
const GLfloat maxddxzt	= 5;
const GLfloat maxddyzt	= 5;
const GLfloat maxddxyt	= 0.05;	//repurposed^


/******************************************************************************
Handle drawing. All functions changing what is drawn will only change variables
and perspectives used by this function.

@param - none;
@return - Current frame of dancing drawn;
******************************************************************************/
void display(void)
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f (1.0, 1.0, 1.0);
	glLoadIdentity ();											//clear the matrix 
		
	//eye does not move/relocate, world will move. Note Z is UP
	//because thats how the mountains were simply drawn
	gluLookAt (ex, ey, ez, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0);	//viewing transformation 

	//movement of world
	glPushMatrix();

	glTranslatef((px+=dpx), 0.0, 0.0);
		
		//auto-move and rotate when roll/pitch
		//care needed when calculating, because we are actually moving world

		//The order of the following is important, see README
		glTranslatef(ex, ey, ez);	 
			//rotate (turn in conjunction with roll) Z'
			//glRotatef((xyt+=dxyt), fx, fy, fz);//THIS FEATURE IS DISORIENTING AND THIS OFF
			//rotate (pitch) Y'
			glRotatef((xzt+=dxzt), gx, gy, gz); 
			//rotate (roll) X' 
			glRotatef((yzt+=dyzt), evx, evy, evz);
		glTranslatef(-ex, -ey, -ez);


		
		glPushMatrix();
			glScalef(20, 20, 2);
			drawMatrix();
		glPopMatrix();
		
	glPopMatrix();


	//natrual decay
	if(dxyt<0)//Z' rotation
	{
		dxyt+=ddxyt/2;
	}
	else
	{
		if(dxyt>0)
		{
			dxyt-=ddxyt/2;
		}
	}
	if(dyzt<0)//X' rotation
	{
		dyzt+=ddyzt/2;
	}
	else
	{
		if(dyzt>0)
		{
			dyzt-=ddyzt/2;
		}
	}
	if(dxzt<0)//Y' rotation
	{
		dxzt+=ddxzt/2;
	}
	else
	{
		if(dxzt>0)
		{
			dxzt-=ddxzt/2;
		}
	}
	
	glutSwapBuffers();
	glFlush ();
	glutPostRedisplay();
	return;
}

void drawMatrix()
{
	GLfloat tanamb[] = {0.2,0.15,0.1,1.0};
	GLfloat tandiff[] = {0.4,0.3,0.2,1.0};

	GLfloat seaamb[] = {0.0,0.0,0.2,1.0};
	GLfloat seadiff[] = {0.0,0.0,0.8,1.0};
	GLfloat seaspec[] = {0.5,0.5,1.0,1.0};


	glTranslatef (-0.5, -0.5, 0.0);							//modeling transformation

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, tanamb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, tandiff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, tandiff);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 10.0);

	mountain(0.0,0.0,0.0, 1.0,0.0,0.0, 0.0,1.0,0.0, 1.0);
	mountain(1.0,1.0,0.0, 0.0,1.0,0.0, 1.0,0.0,0.0, 1.0);

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, seaamb);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, seadiff);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, seaspec);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 10.0);

	glNormal3f(0.0,0.0,1.0);
	glBegin(GL_QUADS);
		glVertex3f(0.0,0.0,sealevel);
		glVertex3f(1.0,0.0,sealevel);
		glVertex3f(1.0,1.0,sealevel);
		glVertex3f(0.0,1.0,sealevel);
	glEnd();


	return;
}



/******************************************************************************
Handles animation by adjusting variables every FPS times a second

@param - dummy input required by callback
@return - display will draw adjusted figure;
******************************************************************************/
void animation(int dummy)
{
	glutPostRedisplay();
	glutTimerFunc(1000/FPS, animation, dummy); // restart timer again
	return;
}

/******************************************************************************
Keyboard interactivity.

@param - keyboard key triggering event;
@return - (My added ones only listed:
	Space	=	Switch between outline and color-filled modes;
	'q'		
	'Q'		=	terminate program;

	{w,a,s,d}
	{W,A,S,D}
	{UP,DOWN,LEFT,RIGHT}	=	directional navigation (via pitch and roll)

	default	=	cout entered key
******************************************************************************/
void keyboard(unsigned char key, int x, int y)
{
   switch (key)
   {
		case '-':
			sealevel -= 0.01;
			break;
		case '+':
		case '=':
			sealevel += 0.01;
			break;
		case 'f':
			polysize *= 0.5;
			break;
		case 'c':
			polysize *= 2.0;
			break;
		case 27:
			exit(0);
			break;

		case 'w':
		case 'W':
			specialKey(GLUT_KEY_UP, x, y);
			break;
		case 'a':
		case 'A':
			specialKey(GLUT_KEY_LEFT, x, y);
			break;
		case 's':
		case 'S':
			specialKey(GLUT_KEY_DOWN, x, y);
			break;
		case 'd':
		case 'D':
			specialKey(GLUT_KEY_RIGHT, x, y);
			break;

		case ' ':
			colorFilled =! colorFilled;
			break;
		default:
			cout << key << endl;
			break;
   }


   return;
}

void specialKey(int key, int x, int y)
{
	switch (key)
   {
		case GLUT_KEY_UP:
			if((dxzt+=ddxzt) > maxddxzt)//Y'
			{
				dxzt=maxddxzt;
			}
			break;
		case GLUT_KEY_LEFT:
			if((dxyt-=ddxyt) < -maxddxyt)//Z' NOPE REPURPOSED
			{
				dxyt=-maxddxyt;
			}
			if((dyzt+=ddyzt) > maxddyzt)//X'
			{
				dyzt=maxddyzt;
			}
			break;
		case GLUT_KEY_DOWN:
			if((dxzt-=ddxzt) < -maxddxzt)//Y'
			{
				dxzt=-maxddxzt;
			}
			break;
		case GLUT_KEY_RIGHT:
			if((dxyt+=ddxyt) > maxddxyt)//Z' NOPE REPURPOSED
			{
				dxyt=maxddxyt;
			}
			if((dyzt-=ddyzt) < -maxddyzt)//X'
			{
				dyzt=-maxddyzt;
			}
			break;
		default:
			cout << key << endl;
			break;
   }
}


/******************************************************************************
MAIN
******************************************************************************/
int main(int argc, char** argv)
{
	init(argc, argv);


	glutDisplayFunc(display); 
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKey);

	glutTimerFunc(100, animation, 1);


	glutMainLoop();
	return 0;
}


int seed(float x, float y)
{
    static int a = 1588635695, b = 1117695901;
	int xi = *(int *)&x;
	int yi = *(int *)&y;
    return ((xi * a) % b) - ((yi * b) % a);
}


void mountain(	float x0, float y0, float z0, 
				float x1, float y1, float z1, 
				float x2, float y2, float z2, float s)
{
	float x01,y01,z01,x12,y12,z12,x20,y20,z20;
	if(s < polysize)
	{
		x01 = x1 - x0;
		y01 = y1 - y0;
		z01 = z1 - z0;

		x12 = x2 - x1;
		y12 = y2 - y1;
		z12 = z2 - z1;

		x20 = x0 - x2;
		y20 = y0 - y2;
		z20 = z0 - z2;

		float nx = y01*(-z20) - (-y20)*z01;
		float ny = z01*(-x20) - (-z20)*x01;
		float nz = x01*(-y20) - (-x20)*y01;

		float den = sqrt(nx*nx + ny*ny + nz*nz);

		if (den > 0.0)
		{
			nx /= den;
			ny /= den;
			nz /= den;
		}

		glNormal3f(nx,ny,nz);
		glBegin(GL_TRIANGLES);
			glVertex3f(x0,y0,z0);
			glVertex3f(x1,y1,z1);
			glVertex3f(x2,y2,z2);
		glEnd();

		return;
	}

	x01 = 0.5*(x0 + x1);
	y01 = 0.5*(y0 + y1);
	z01 = 0.5*(z0 + z1);

	x12 = 0.5*(x1 + x2);
	y12 = 0.5*(y1 + y2);
	z12 = 0.5*(z1 + z2);

	x20 = 0.5*(x2 + x0);
	y20 = 0.5*(y2 + y0);
	z20 = 0.5*(z2 + z0);

	s *= 0.5;

	srand(seed(x01,y01));
	z01 += 0.3*s*(2.0*((float)rand()/(float)RAND_MAX) - 1.0);
	srand(seed(x12,y12));
	z12 += 0.3*s*(2.0*((float)rand()/(float)RAND_MAX) - 1.0);
	srand(seed(x20,y20));
	z20 += 0.3*s*(2.0*((float)rand()/(float)RAND_MAX) - 1.0);

	mountain(x0,y0,z0,x01,y01,z01,x20,y20,z20,s);
	mountain(x1,y1,z1,x12,y12,z12,x01,y01,z01,s);
	mountain(x2,y2,z2,x20,y20,z20,x12,y12,z12,s);
	mountain(x01,y01,z01,x12,y12,z12,x20,y20,z20,s);
}


/******************************************************************************
Establish background color. For this assignment, this function doesn't need to
do anything else.

@param - none;
@return - default background color established;
******************************************************************************/
void init(int argc, char** argv) 
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//center screen and give it default size.
	int w=960;
	int h=540;
	glutInitWindowSize(w, h);
	glutInitWindowPosition(	glutGet(GLUT_SCREEN_WIDTH)/2-w/2,
							glutGet(GLUT_SCREEN_HEIGHT)/2-h/2);
	glutCreateWindow(argv[0]);


	GLfloat white[] = {1.0,1.0,1.0,1.0};
	GLfloat lpos[] = {0.0,1.0,0.0,0.0};

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT0, GL_POSITION, lpos);
	glLightfv(GL_LIGHT0, GL_AMBIENT, white);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white);

	glClearColor (0.5, 0.5, 1.0, 0.0);
	/* glShadeModel (GL_FLAT); */
	glEnable(GL_DEPTH_TEST);

	sealevel = 0.0;
	polysize = 0.01;
}


/******************************************************************************
Changes perspective to ensure display draws scaled to the window.

@param - none;
@return - gluPerspective adjusted;
******************************************************************************/
void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluPerspective(90.0,1.0,0.01,10.0);
	glMatrixMode (GL_MODELVIEW);

	
	return;
}


