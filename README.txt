This is a flight simulator, it is the bare minimum, implemented by creating a coordinate system using the eye vector as the X'. It is a first person perspective, which was stated as fine on piazza. There is minimal terrain because rerendering it for large areas was too expensive.

This program was initially designed so that when you roll, you also curve towards that direction instead of simply rotating on the axis lining your plane. This was VERY disorienting and had to go. Now it does the standard, spin, then pitch to determine direction.

The terrain seems to spontainiously disappear at some points. I know I am not getting lost since I'll simply hold a pitch button, but I will never get back to the terrain. This error has not been solved, and its cause has not been identified. It may be machine specific.

The Video is included in two parts. Part 1 demonstrates without velocity first, to demonstrate the plane does indeed roll and pitch. and both simultainiously. Part 2 runs in slow motion while including constant forward motion to quickly demo the features before the plane escapes the small terrain, to show that it works. After which, in part 2, a very quick demo will also be done, just to show standard speeds do indeed work.

'esc' exits, {w,a,s,d} and arrow keys all work for the intuitive implemetation for directions. Note that UP_KEY pitches DOWN, and vice versa.